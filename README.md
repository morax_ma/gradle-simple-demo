# Gradle 多模块管理简单示例
## 包结构
### common
* 常用类包:常量、工具类
### model
* 数据库相关包:实体,通用数据库操作Service
### core
* 核心业务包:业务Service
### api
* 提供Rest接口